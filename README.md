# [GitLab CI](https://docs.gitlab.com/ee/ci/) Tests

## Copying

🅭🄍1.0: This repo is dedicated to the public domain using
[the CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
